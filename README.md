# springboot-app

## 系统介绍

- springboot-app 是J2EE基础开发平台，架构设计包括（动静分离），技术栈包括（SpringBoot、MyBatis、Shiro、easyui），业务模块包括：用户管理，角色管理、权限管理，字典管理。

## 业务功能

- 1.用户管理：用户是系统操作者，该功能主要完成系统用户配置。
- 2.角色管理：角色菜单权限分配。
- 3.菜单管理：配置系统菜单，操作权限，按钮权限标识等。
- 4.字典管理：对系统中经常使用的一些较为固定的数据进行维护，如：是否、男女、类别、级别等。
- 5.定时器：定时跑批程序管理，包括新建，更改，删除，暂停，执行一次，监控

## 技术栈

- 核心 springboot 
- MVC springmvc 
- ORM mybatis 
- 权限 shiro 
- 验证 hibernate-validation
- 连接池 druid
- 页面UI easyui
- 构建 maven
- 容器 tomcat
- 数据库 mysql
- 定时任务 quartz


## 部署

- 1.导入数据库脚本springboot-app.sql
- 2.启动sprtingboot-app位置com.wangsong.Application
- 3.访问/springboot-app/html/login.html


## qq交流群

- 74745979